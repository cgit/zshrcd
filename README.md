# Rahm's Zsh Files.

This is my hand-rolled ZSH configuration system.

It's pretty simple how it works. Each of the `*.zsh` files in this directory are
sourced in alphabetical order by the `zshrc` file.

Local configurations are stored in the `local/ directory`. The same rules apply;
however, these files are covered by the `.gitignore` and thus are designed to be
local-only configuration that is not committed to git.

Prompts are selected based no the hostname and are stored in the `prompts/`
folder. Specifically, `prompts/$(hostname).zsh` is sourced.

## Installation

```sh
git clone git://git.josher.dev/zshrcd.git ~/.zshrc.d && \
    ln -s ~/.zshrc.d/zshrc ~/.zshrc
```
