alias ls='ls --color=auto'
alias no=ls

alias copy="xclip -selection c -i"
alias put="xclip -selection c -o"

whence exa > /dev/null
if [ "$?" -eq 0 ] ; then
  alias 'ls=exa'
fi
