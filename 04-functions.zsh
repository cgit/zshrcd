if [ -n '$TMUX' ]; then
  function refresh {
    export SSH_AUTH_SOCK="$(tmux show-environment | grep '^SSH_AUTH_SOCK'  | cut -d '=' -f 2)"
    export SSH_AUTH_KEY="$(tmux show-environment | grep '^SSH_AUTH_KEY'	  | cut -d '=' -f 2)"
    export SSH_CONNECTION="$(tmux show-environment | grep '^SSH_CONNECTION' | cut -d '=' -f 2)"
    export DISPLAY="$(tmux show-environment | grep '^DISPLAY'	     | cut -d '=' -f 2)"
  }
else
  function refresh { }
fi

# declare a list of expandable aliases to fill up later
typeset -a ealiases
ealiases=()

# write a function for adding an alias to the list mentioned above
function abbrev-alias() {
    alias $1
    ealiases+=(${1%%\=*})
}

# expand any aliases in the current line buffer
function expand-ealias() {
    if [[ $LBUFFER =~ "\<(${(j:|:)ealiases})\$" ]]; then
        zle _expand_alias
        zle expand-word
    fi
    zle magic-space
}
zle -N expand-ealias

# Bind the space key to the expand-alias function above, so that space will expand any expandable aliases
bindkey ' '        expand-ealias
bindkey '^ '       magic-space     # control-space to bypass completion
bindkey -M isearch " "      magic-space     # normal space during searches

# A function for expanding any aliases before accepting the line as is and executing the entered command
expand-alias-and-accept-line() {
    expand-ealias
    zle .backward-delete-char
    zle .accept-line
}

zle -N accept-line expand-alias-and-accept-line

abbrev-alias g="git"
abbrev-alias gst="git status"
abbrev-alias gcb="git checkout --branch"
abbrev-alias ll="ls -l"
