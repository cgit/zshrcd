bindkey -v

insert-last-word() {
    # Trim trailing whitespace from LBUFFER
  local trimmed="${LBUFFER%"${LBUFFER##*[![:space:]]}"}"

  # Extract the last space-separated word
  local last_word="${trimmed##* }"
  LBUFFER+="$last_word"
}

zle -N insert-last-word

bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

bindkey -s '[32;2u' ' '
bindkey '[127;2u' backward-delete-char
bindkey -s '[225;2u' ' '

bindkey -v '^?' backward-delete-char
bindkey -v '' backward-delete-char

autoload edit-command-line
zle -N edit-command-line
bindkey '' edit-command-line

bindkey '' insert-last-word

# This allows a ctrl-w motion with neovim, so one does not have do a terminal
# escape to switch panes.
nvim-move-pane() {
  read -sk key
  nvimctl feedkeys "$key"
}

# Changes the tab in the parent Neovim process.
nvim-move-tab() {
  nvimctl feedkeys "gt"
}

# Changes the tab in the parent Neovim process.
nvim-move-tab-back() {
  nvimctl feedkeys "gT"
}

# Does anyone actually use the execute prompt in zsh? Rebind to go directly to
# the neovim command prompt.
nvim-cmd-mode() {
  if [[ -z "$NVIM" ]] ; then
    # fallback to original behavior.
    zle execute-named-cmd
  else
    nvimctl feedkeys ":"
  fi
}

quote-escape() {
  sed "s/'/'\"'\"'/g;s/\\\\/\\\\\\\\/g" <<< "$1"
}

expand-last-file() {
  local before_cursor=$LBUFFER
  local after_cursor=$RBUFFER

  local after_cursor_first_word=${after_cursor/ *}
  local after_cursor_remaining=${after_cursor[${#after_cursor_first_word}+1,-1]}

  local before_cursor_last_word=${before_cursor//* }
  local before_cursor_remaining=${before_cursor[0, -${#before_cursor_last_word}-1]}

  local file="${before_cursor_last_word}${after_cursor_first_word}"

  if [ -f "${file}" ] ; then
    local catted_file="$(cat ${file})"
    catted_file="'$(quote-escape "${catted_file}")'"

    local new_l_buffer="${before_cursor_remaining}${catted_file}"

    BUFFER="${new_l_buffer}${after_cursor_remaining}"
    CURSOR=$#new_l_buffer
  fi
}

select-nvim-reg() {
  read -sk reg
  ZSH_VIM_REGISTER_SEL="$reg"
}

put-after-reg-override() {
  reg_sel="$ZSH_VIM_REGISTER_SEL"
  ZSH_VIM_REGISTER_SEL=""
  if [ -z "$NVIM" ] || [ -z "$reg_sel" ] ; then
    zle vi-put-after
  else
    new_l_buffer="${BUFFER[0,$CURSOR+1]}"
    new_l_buffer="${new_l_buffer}$(nvimctl getreg "$reg_sel")"
    new_r_buffer="${BUFFER[$CURSOR+2,-1]}"

    BUFFER="${new_l_buffer}$new_r_buffer"
    CURSOR=$#new_l_buffer
  fi
}

nvim-complete-lst() {
  compadd "lol" "hi" "you" "apples"
  compadd "good" "day" "sir"
}

nvim-viins-ctrl-w() {
  if [[ "$BUFFER" == "" ]] ; then
    zle vi-cmd-mode
    nvim-move-pane
  else
    zle vi-backward-kill-word
  fi
}

zle -C nvim-complete menu-select nvim-complete-lst
zle -C files-complete menu-select _files

zle -N nvim-move-tab
zle -N nvim-move-tab-back
zle -N nvim-move-pane
zle -N nvim-cmd-mode
zle -N expand-last-file
zle -N select-nvim-reg
zle -N put-after-reg-override
zle -N nvim-viins-ctrl-w

bindkey -M vicmd 'gt' nvim-move-tab
bindkey -M vicmd 'gT' nvim-move-tab-back
bindkey -M vicmd '' nvim-move-pane
bindkey -M vicmd ':' nvim-cmd-mode
bindkey -M viins '' expand-last-file
bindkey -M viins '' nvim-viins-ctrl-w
bindkey -M viins '' files-complete

# Allow selecting and pasting registers from neovim using @<reg>p
bindkey -M vicmd '@' select-nvim-reg
bindkey -M vicmd 'p' put-after-reg-override

bindkey -M viins '' nvim-complete
bindkey -M viins '' vi-forward-char

