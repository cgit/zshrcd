# Loads the other files in order. This file should not be touched. It is used
# simply as a way to load the other files in the .zshrc.d folder.

for f in $HOME/.zshrc.d/*.zsh ; do
  source "$f"
done || true
