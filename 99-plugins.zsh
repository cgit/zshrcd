source ~/.zshrc.d/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

fpath=(~/.zshrc.d/plugins/completions/ $fpath)

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

for f in ~/.zshrc.d/local/*.zsh ; do
  source "$f"
done 2>/dev/null

if [ -z "$USE_FAST_SYNTAX" ] ; then
  source ~/.zshrc.d/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
else
  source ~/.zshrc.d/plugins/fast-syntax-highlighting/F-Sy-H.plugin.zsh
fi

if ( which zoxide &> /dev/null ) ; then
  eval "$(zoxide init zsh)"
  alias cd=z
fi

/bin/true
