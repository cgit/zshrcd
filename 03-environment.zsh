export PATH="$HOME/.cargo/bin:$HOME/.local/bin:$PATH"

if [[ "$TERM" == "alacritty" ]] ; then
  export TERM=xterm-256color
fi

if ( which luarocks &>/dev/null ) ; then
  eval $(luarocks path)
fi

export LANG=en_US.UTF-8
export EDITOR=nvim

if [ -x luarocks ] ; then
  eval $(luarocks path)
fi
