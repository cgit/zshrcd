PURPLE="%{\x1b[01;35m%}"
RED="%{\x1b[01;31m%}"
RESET="%{\x1b[0m%}"

if [[ "$(hostname)" == *strange* ]] ; then
  color1=$RED
  color2=$PURPLE
else
  color1=$PURPLE
  color2=$RED
fi

function wrap {
  echo "${color1}[$1${color1}]\$${RESET} "
}

PROMPT_END=" "
PROMPT_REND=" "

PROMPT='$(wrap "$(simple_prompt $color1 $color2)")'
RPROMPT='$(simple_rprompt $color1 $color2)'
