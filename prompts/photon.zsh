grey='\e[0;90m'

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[cyan]%}["
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="]%{$fg[red]%}▲%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="] "

GREEN="%{\x1b[01;32m%}"
PURPLE="%{\x1b[01;35m%}"
GRAY="%{\x1b[01;38;5;238m%}"
ORANGE="%{\x1b[01;38;5;166m%}"
YELLOW="%{\x1b[01;33m%}"
RED="%{\x1b[01;31m%}"
RESET="%{\x1b[00m%}"

terminal_color_1="${ORANGE}"
terminal_color_2="${YELLOW}"
terminal_color_3="${ORANGE}"
terminal_color_4="${YELLOW}"
terminal_color_5="${YELLOW}"
terminal_color_6="${YELLOW}"
terminal_color_7="${YELLOW}"

function rahm_prompt {
  power_prompt "$?"
}

function rahm_rprompt {
  power_prompt "$?" r
}

setopt prompt_subst

PROMPT='$(rahm_prompt)'
RPROMPT='$(rahm_rprompt)'
