ORANGE="%{\x1b[01;38;5;208m%}"
GRAY="%{\x1b[01;38;5;239m%}"

PROMPT='$(simple_prompt ${ORANGE} ${GRAY})'
RPROMPT='$(simple_rprompt ${ORANGE} ${GRAY})'
