PURPLE="%{\x1b[01;35m%}"
RED="%{\x1b[01;31m%}"
BLUE="%{\x1b[01;34m%}"
GREEN="%{\x1b[01;32m%}"
VOID="%{\x1b[01;38;2;84;139;84m%}"
RESET="%{\x1b[0m%}"

#545A57

color1=$BLUE
color2=$VOID

function wrap {
  echo "${color1}$1${color1})\$${RESET} "
}
function rwrap {
  echo "${color1}($1${color1}${RESET}"
}

PROMPT_END=" "
PROMPT_REND=" "

PROMPT='$(wrap "$(simple_prompt $color1 $color2)")'
RPROMPT='$(rwrap "$(simple_rprompt $color1 $color2)")'
