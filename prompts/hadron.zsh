GREEN="%{\x1b[01;32m%}"
PURPLE="%{\x1b[01;35m%}"

PROMPT_END=" · "
PROMPT_REND=" · "

PROMPT='$(simple_prompt ${GREEN} ${PURPLE})'
RPROMPT='$(simple_rprompt ${GREEN} ${PURPLE})'
