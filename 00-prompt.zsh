hostname=$( which hostname &>/dev/null && hostname || cat /etc/hostname )

# Creates simple prompts for my many machines.  #
# The first argument to the prompts is the primary color
# second argument is secondary color.

GREEN="%{\x1b[01;32m%}"
RED="%{\x1b[01;31m%}"
RESET="%{\x1b[00m%}"

function distro_logo {
  case "$(uname -a)" in
    *-arch*)  echo "%{\033[01;38;2;21;118;167m%} $RESET" ;;
    *Debian*) echo "%{\033[01;38;2;170;6;64m%}$RESET" ;;
    *Ubuntu*) echo "%{\033[01;38;2;172;61;24m%} $RESET" ;;
    *fc*)     echo "%{\033[01;38;2;21;118;167m%} $RESET" ;;
    *gentoo*) echo "%{\033[01;38;2;126;126;183m%} $RESET" ;;
    *Linux*)
      case "$(lsb_release -si)" in
        *Void*) echo "%{\033[01;38;2;71;128;97m%}◯ $RESET" ;;
      esac ;;
    *BSD*)    echo "%{\033[01;31m%} $RESET" ;;
  esac
}

DISTRO_LOGO=$(distro_logo)

function simple_prompt {
  ec=$?
  preamble=""

  default="${2}"
  if [ "$ec" -eq "0" ] ; then
    code="${GREEN}--${default}"
  else
    code="${RED}$(printf '%02x' $ec)${default}"
  fi

  if [ -z "$PROMPT_END" ] ; then
    PROMPT_END=" → "
  fi

  name="${1}%n@${default}"
  host="${1}%m${default}"
  directory="${2}%2~${1}"

  echo " ${default}${DISTRO_LOGO} ${code} ${name}${host} ${directory}${PROMPT_END}${RESET}"
}

function simple_rprompt {
  if [ -z "$PROMPT_REND" ] ; then
    PROMPT_REND=" ← "
  fi

  default="${2}"
  time="${2}%*${default}"
  echo "${1}${PROMPT_REND}${time} ${RESET}"
}

setopt prompt_subst

# Source the actual prompt file corresponding to this host.

source "$HOME/.zshrc.d/prompts/${hostname}.zsh" 2>/dev/null || \
  source "$HOME/.zshrc.d/local/prompts/${hostname}.zsh" 2>/dev/null

PROMPT='%{[5 q%}'"$PROMPT"
